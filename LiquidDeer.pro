TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CFLAGS += -std=c99

SOURCES += \
    track.cpp \
    pugi/pugixml.cpp \
    deer.cpp \
    xml.cpp \
    sync.cpp \
    parse.cpp \
    interact.cpp \
    auth.cpp \
    gumbo/attribute.c \
    gumbo/char_ref.c \
    gumbo/error.c \
    gumbo/parser.c \
    gumbo/string_buffer.c \
    gumbo/string_piece.c \
    gumbo/tag.c \
    gumbo/tokenizer.c \
    gumbo/utf8.c \
    gumbo/util.c \
    gumbo/vector.c \
    config.cpp


INCLUDEPATH += $$PWD/../../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../usr/lib/x86_64-linux-gnu



HEADERS += \
    track.h \
    pugi/pugiconfig.hpp \
    pugi/pugixml.hpp \
    xml.h \
    sync.h \
    parse.h \
    interact.h \
    auth.h \
    gumbo/gumbo.h \
    gumbo/attribute.h \
    gumbo/char_ref.h \
    gumbo/error.h \
    gumbo/insertion_mode.h \
    gumbo/parser.h \
    gumbo/string_buffer.h \
    gumbo/string_piece.h \
    gumbo/token_type.h \
    gumbo/tokenizer_states.h \
    gumbo/tokenizer.h \
    gumbo/utf8.h \
    gumbo/util.h \
    gumbo/vector.h \
    config.h


unix:!macx: LIBS += -lcurl

unix:!macx: LIBS += -ljansson
